package com.zuitt.sample;

import java.util.Scanner;

public class Person {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("First Name:");
        String fName = input.nextLine();
        System.out.println("Last Name:");
        String lName = input.nextLine();
        System.out.println("First Subject Grade:");
        double grade1 = input.nextDouble();
        System.out.println("Second Subject Grade:");
        double grade2 = input.nextDouble();
        System.out.println("Third Subject Grade:");
        double grade3 = input.nextDouble();

        int average = (int) ((grade1 + grade2 + grade3)/3);
        System.out.println("Good day, " + fName + " " +  lName + ".\nYour grade average is: " + average);
    }
}
